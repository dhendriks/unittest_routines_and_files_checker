"""
Functions to handle checking whether the correct functions are covered in tests explicitly (not implied by the coverage)

TODO: make function to report on missing tests
TODO: make function that finds test files that are not accompanied by source files
TODO: provide suggestions if test functions are missing but may be present in other files.
TODO: before making the test file, prompt the user whether they want that.
"""

import os

from unittest_routines_and_files_checker.test_file_template import test_file_template

EXCLUDE_KEYWORD = "DH0001"
EXCLUDE_FILE_KEYWORD = "DH0001_file"


def clean_routine_name(line, routine_keyword):
    cleaned_routine_name = line[len(routine_keyword):].split('(')[0].strip()

    return cleaned_routine_name

def compare_structure(file_list):
    """
    
    """

def _compare_routines(source_routines, test_routines):
    """
    Function that compares the routines between source and test file
    """


    # 
    missing_from_testroutines = []
    for routine in source_routines:
        if 'test_'+routine not in test_routines:
            missing_from_testroutines.append(routine)

    # 
    missing_from_routines = []
    for routine in test_routines:
        if routine[len("test_"):] not in source_routines:
            missing_from_routines.append(routine)

    return missing_from_testroutines, missing_from_routines


def compare_routines(file_list):
    """
    Function to compare the routines with their respective testroutines
    """

    for file in file_list:

        #
        file['missing_from_testroutines'], file['missing_from_routines'] = _compare_routines(
            source_routines=file['routines'],
            test_routines=file['test_routines']
        )


    return file_list

def extract_routine_names(filename, routine_keyword, blacklist=[], routine_required_prepend=""):
    """
    Function to extract routine names in a file
    """

    routine_names = []

    with open(filename, 'r') as f:
        for line in f:
            cleaned_line = line.strip()

            #######
            # 
            if EXCLUDE_FILE_KEYWORD in cleaned_line:
                return []

            #######
            # if commented
            if cleaned_line.startswith("#"):
                continue

            # if flagged to exclude
            if EXCLUDE_KEYWORD in cleaned_line:
                continue

            #######
            # Catch routine
            if cleaned_line.startswith(routine_keyword):
                # clean routine name
                cleaned_routine_name = clean_routine_name(
                    line=cleaned_line,
                    routine_keyword=routine_keyword
                )

                if not cleaned_routine_name in blacklist:
                    # if routine_required_prepend and cleaned_routine_name.startswith(routine_required_prepend):
                    # TODO: fix that test routines need to start with test
                    routine_names.append(cleaned_routine_name)

    return routine_names

def find_structure(root_dir, file_list=None, module_list=None):
    """
    Function to explore the sub-directory tree and list all files and which module they belong to
    """

    #
    file_blacklist = ["__init__.py", "archive.py", '_version.py']
    module_blacklist = ['tests', 'example_data', '__pycache__']
    if file_list is None:
        file_list = []
    if module_list is None:
        module_list = []

    ##########
    # 
    content = os.listdir(root_dir)

    # loop over content dir
    for el in content:
        full_name = os.path.join(root_dir, el)

        # check if file
        if os.path.isfile(full_name):
            if el in file_blacklist:
                continue
            if el.endswith('py'):
                # check if there is something in the file to exclude it
                with open(full_name) as f:
                    for line in f:
                        cleaned_line = line.strip()
                        if EXCLUDE_FILE_KEYWORD in cleaned_line:
                            continue

                #
                file_list.append({
                        'full_filename': full_name,
                        'module': module_list,
                        'basename': el,
                        'relative_name': os.path.join(*module_list, el)
                    }
                )

        # check if directory
        if os.path.isdir(full_name):
            if not el in module_blacklist:
                find_structure(root_dir=os.path.join(root_dir, el), file_list=file_list, module_list=[*module_list, el])

    return file_list


def generate_test_structure(test_root, file_list):
    """
    Function to generate the structure of the test files by adding entries to each file dictionary
    """

    for file in file_list:

        # add testfile basename
        file['test_basename'] = "test_" + file['basename']

        # Add testfile dirname
        file['test_dirname'] = os.path.join(test_root, *["tests_"+el for el in file['module']])

        # Add testfile fullname
        file['test_full_name'] = os.path.join(file['test_dirname'], file['test_basename'])

        # Add relative name
        file['test_relative_name'] = os.path.join(*["tests_"+el for el in file['module']], file['test_basename'])


    return file_list


def create_test_structure(file_list):
    """
    Function to create the test files if they do not already exist
    """

    #
    for file in file_list:
        # create directory
        if not os.path.isdir(file['test_dirname']):
            os.makedirs(file['test_dirname'])

        # create testfile
        if not os.path.isfile(file['test_full_name']):
            # write template
            with open(file['test_full_name'], 'w') as f:
                formatted_template = test_file_template.format(relative_name=file['relative_name'])

                #
                f.write(
                    formatted_template
                )

    return file_list

def add_routine_names(file_list):
    """
    Function to add the routine names to the file dict
    """

    #
    for file in file_list:
        routines = extract_routine_names(file['full_filename'], routine_keyword='def ')

        file['routines'] = routines

    return file_list

def add_testroutine_names(file_list):
    """
    Function to add the test routine names to the file dict
    """

    for file in file_list:
        routines = extract_routine_names(file['test_full_name'], routine_keyword='class ', routine_required_prepend='test')

        file['test_routines'] = routines

    return file_list


def create_testfunction_dict(file_list):
    """ 
    Function that takes the file list and returns a large dictionary that contains function names as keys, and a list of filenames as values
    """

    grand_testfunction_dict = {}

    for file in file_list:
        full_filename = file['test_full_name']
        # full_filename = os.path.join(*file['module'], file['basename'])
        for testroutine in file['test_routines']:
            if not testroutine in grand_testfunction_dict:
                grand_testfunction_dict[testroutine] = []
            grand_testfunction_dict[testroutine].append(full_filename)

    # remove those with empty lists (just to be sure)
    for routine in grand_testfunction_dict.keys():
        if not grand_testfunction_dict[routine]:
            del grand_testfunction_dict[routine]

    return grand_testfunction_dict


def report_on_missing_tests(file_list):
    """
    Function to report on missing tests
    """

    #
    total_testroutines = 0
    total_sourceroutines = 0
    total_testroutines_missing = 0
    total_sourceroutines_missing = 0
    files_with_testroutines_missing = 0
    files_with_sourceroutines_missing = 0
    files_with_testroutines = 0
    files_with_sourceroutines = 0

    ##########
    # Create pool of test functions and their associated files
    testfunction_dict = create_testfunction_dict(file_list=file_list)
    # print("testfunction_dict", testfunction_dict)

    # 
    for file in file_list:

        files_with_testroutines += 1
        files_with_sourceroutines += 1

        total_testroutines += len(file['test_routines'])
        total_sourceroutines += len(file['routines'])


        if (not file['missing_from_testroutines']) and (not file['missing_from_routines']):
            continue

        print("###############")
        print("\tfull path source file: {}".format(file['full_filename']))
        print("\tfull path test file: {}".format(file['test_full_name']))
        print("\n")

        ##############
        # print which test routines are missing
        if file['missing_from_testroutines']:
            print("\tsource file {} is missing the following test routines:".format(os.path.join(*file['module'], file['basename'])))
            files_with_testroutines_missing += 1
            ###########
            # Report on what is missing
            for routine in file['missing_from_testroutines']:
                print("\t\t{}".format(routine))
                total_testroutines_missing += 1

                ###########
                # But, for each of the missing functions, see if we can find 
                equivalent_test_routine = "test_{}".format(routine)
                if equivalent_test_routine in testfunction_dict.keys():
                    print("\t\tFunction might be present in one of the following files:")

                    for filename in testfunction_dict[equivalent_test_routine]:
                        print("\t\t\t{}".format(filename))
                    print("\n")
            print("\n")

        if file['missing_from_routines']:
            print("\ttest file {} is missing the following source routines:".format(file['test_relative_name']))
            files_with_sourceroutines_missing += 1
            for routine in file['missing_from_routines']:
                total_sourceroutines_missing += 1
                print("\t\t{}".format(routine))
            print("\n")

    ##########
    # 
    print("In total {} test routines are missing from {} files".format(total_testroutines_missing, files_with_testroutines_missing))
    print("In total {} source routines are missing from {} files".format(total_sourceroutines_missing, files_with_sourceroutines_missing))

    print("Found a total of {} test routines in {} files".format(total_testroutines, files_with_testroutines))
    print("Found a total of {} source routines in {} files".format(total_sourceroutines, files_with_sourceroutines))



def analyse_and_build_test_structure(root_source_files, root_test_files, create_files=True):
    """
    Main function to handle the steps to generate a 
    """

    #
    file_list = find_structure(root_dir=root_source_files)

    #
    file_list = generate_test_structure(test_root=root_test_files, file_list=file_list)

    #
    if create_files:
        file_list = create_test_structure(file_list=file_list)

    #
    file_list = add_routine_names(file_list=file_list)

    #
    file_list = add_testroutine_names(file_list=file_list)

    #
    file_list = compare_routines(file_list=file_list)

    #
    report_on_missing_tests(file_list=file_list)



if __name__=="__main__":
    # # 
    # filename="/home/david/projects/binary_c_root/binarycpython_dev/binary_c-python/binarycpython/utils/convolution/store_redshift_shell_info.py"
    # extract_routine_names(filename=filename, routine_keyword='def')

    # #
    # filename="/home/david/projects/binary_c_root/binarycpython_dev/binary_c-python/binarycpython/tests/tests_convolution/test_prepare_output_file.py"
    # extract_routine_names(filename=filename, routine_keyword='class')

    # #
    # file_list = find_structure(root_dir="/home/david/projects/binary_c_root/binarycpython_dev/binary_c-python/binarycpython/utils")
    # # for file in file_list:
    # #     print(file)

    # # 
    # file_list = generate_test_structure(test_root="/home/david/Desktop/testing_function_tmp/tests", file_list=file_list)
    # # for file in file_list:
    # #     print(file)

    # # 

    # file_list = add_routine_names(file_list=file_list)
    # file_list = add_testroutine_names(file_list=file_list)
    # file_list = compare_routines(file_list=file_list)

    # for file in file_list:
    #     print(file)
    # #     
    # analyse_and_build_test_structure(
    #     root_source_files="/home/david/projects/binary_c_root/binarycpython_dev/binary_c-python/binarycpython/utils",
    #     root_test_files="/home/david/projects/binary_c_root/binarycpython_dev/binary_c-python/binarycpython/tests",
    #     # root_test_files="/home/david/Desktop/testing_function_tmp/tests"
    # )

    analyse_and_build_test_structure(
        root_source_files="/home/david/projects/binary_c_root/binarycpython_dev/binary_c-python/binarycpython/utils/convolution",
        root_test_files="/home/david/projects/binary_c_root/binarycpython_dev/binary_c-python/binarycpython/tests/tests_convolution",
        # root_test_files="/home/david/Desktop/testing_function_tmp/tests"
    )