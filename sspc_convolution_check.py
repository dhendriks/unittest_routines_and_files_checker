from functions import analyse_and_build_test_structure

analyse_and_build_test_structure(
    root_source_files="/home/david/projects/binary_c_root/sspc_convolution/syntheticstellarpopconvolve",
    root_test_files="/home/david/projects/binary_c_root/sspc_convolution/syntheticstellarpopconvolve/tests",
    # root_test_files="/home/david/Desktop/testing_function_tmp/tests"
)